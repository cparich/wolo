import QtQuick 2.12
import QtQml.Models 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as Controls
import QtMultimedia 5.12
import org.kde.kirigami 2.5 as Kirigami
import us.parich.wolo 1.0

Kirigami.Page {
    id: root

    title: '/' + boardId + '/' + threadId

    property string boardId
    property string threadId

    ListModel {
        id: listData
    }

    ThreadRepo {
        id: thread
        boardId: root.boardId
        threadId: root.threadId

        onThreadIdChanged: thread.refresh()
        onBoardIdChanged: thread.refresh()

        onPostListChanged: {
            for (var item in newPosts_)
                listData.append({
                                    "modelData": newPosts_[item]
                                })
        }

        onProgress: root.progress = progress_
    }

    actions {
        main: Kirigami.Action {
            text: "Refresh"
            icon.name: "view-refresh"
            onTriggered: thread.refresh()
        }
        left: Kirigami.Action {
            text: "Top"
            icon.name: "go-top"
            onTriggered: list.currentIndex = 0
        }
        right: Kirigami.Action {
            text: "Bottom"
            icon.name: "go-bottom"
            onTriggered: list.currentIndex = list.count - 1
        }

        contextualActions: [
            Kirigami.Action {
                text: "Bookmark"
                icon.name: "bookmark"
            },
            Kirigami.Action {
                text: "Reply"
                icon.name: "edit"
            }
        ]
    }

    Kirigami.OverlayDrawer {
        id: threadPopup

        edge: Qt.RightEdge
        handleVisible: false

        width: applicationWindow().width * 0.9

        property string postId: ''
        property var replies: thread.replies(postId)
        property var postStack: []

        onPostIdChanged: {
            backButton.visible = postStack.length > 0
        }

        onDrawerOpenChanged: {
            if (!drawerOpen)
                postStack = []
        }

        contentItem: ColumnLayout {
            RowLayout {
                Controls.Button {
                    id: backButton
                    icon.name: 'back'
                    onClicked: {
                        threadPopup.postId = threadPopup.postStack.pop()
                    }
                }

                Kirigami.Heading {
                    text: threadPopup.replies.length + " replies to " + threadPopup.postId
                    level: 1
                }
            }

            ListView {
                Layout.fillHeight: true
                Layout.fillWidth: true
                model: threadPopup.replies
                delegate: postItem
            }
        }
    }

    Component {
        id: postItem

        Item {
            readonly property string postId: modelData
            readonly property Post post: thread.post(postId)

            implicitHeight: ly.height
            width: parent.width

            GridLayout {
                id: ly

                rowSpacing: Kirigami.Units.largeSpacing
                columnSpacing: Kirigami.Units.largeSpacing
                columns: 3
                rows: 2
                width: parent.width

                Image {
                    id: im
                    source: post.imageId === '' ? '' : 'https://i.4cdn.org/%1/%2s.jpg'.arg(
                                                      root.boardId).arg(
                                                      post.imageId)
                    visible: post.imageId !== ''
                    Layout.maximumWidth: Kirigami.Units.iconSizes.huge
                    Layout.maximumHeight: Kirigami.Units.iconSizes.huge
                    Layout.rowSpan: 3
                    Layout.row: 0
                    Layout.column: 0
                    Layout.alignment: Qt.AlignTop
                    fillMode: Image.PreserveAspectCrop

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if (post.extension === '.webm')
                                pageStack.push(Qt.resolvedUrl(
                                                   'VideoPlayer.qml'), {
                                                   "boardId": root.boardId,
                                                   "file": post.imageId,
                                                   "ext": post.extension
                                               })
                            else
                                pageStack.push(Qt.resolvedUrl(
                                                   'ImageViewer.qml'), {
                                                   "boardId": root.boardId,
                                                   "file": post.imageId,
                                                   "ext": post.extension
                                               })
                        }
                    }
                }

                Kirigami.Heading {
                    text: post.postId + ' : ' + post.subject
                    level: 2
                    wrapMode: Text.WordWrap
                    Layout.columnSpan: 2
                    Layout.row: 0
                    Layout.column: 1
                    Layout.fillWidth: true
                }

                ColumnLayout {
                    Layout.columnSpan: 2
                    Layout.row: 1
                    Layout.column: 1
                    Layout.fillWidth: true
                    Repeater {

                        model: post.body.split('\n')

                        delegate: Controls.Label {
                            readonly property bool isReply: modelData[0] == '>'
                                                            && modelData[1] == '>'
                            readonly property bool isQuote: modelData[0] == '>'
                                                            && !isReply
                            color: isReply ? "red" : (isQuote ? "green" : Kirigami.Theme.textColor)

                            text: modelData
                            wrapMode: Text.Wrap
                            Layout.fillWidth: true

                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    if (!isReply)
                                        return

                                    var target = thread.indexOf(modelData)
                                    if (target === -1)
                                        return
                                    list.currentIndex = target
                                }
                            }
                        }
                    }
                }

                Controls.Button {
                    readonly property int replyCount: post.replies.length
                    text: replyCount + " replies"
                    Layout.columnSpan: 2
                    Layout.row: 2
                    Layout.column: 1
                    visible: replyCount > 0
                    onClicked: {
                        if (threadPopup.drawerOpen)
                            threadPopup.postStack.push(threadPopup.postId)

                        threadPopup.drawerOpen = true
                        threadPopup.postId = post.postId
                    }
                }
            }

            Rectangle {
                height: 1
                color: Kirigami.Theme.textColor
                anchors {
                    left: ly.left
                    right: ly.right
                    bottom: ly.bottom
                }
            }
        }
    }

    Kirigami.CardsListView {
        id: list
        anchors.fill: parent

        rightMargin: 0
        leftMargin: 0

        model: listData

        delegate: postItem

        footer: ColumnLayout {
            anchors.left: parent.left
            anchors.right: parent.right

            Controls.Label {
                text: "Replies: " + thread.replyCount + " Images: " + thread.images
                Layout.alignment: Qt.AlignHCenter
            }
            Controls.Label {
                text: "Refresh in: " + thread.refreshTime
                Layout.alignment: Qt.AlignHCenter
            }
        }
    }
}
