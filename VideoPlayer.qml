import QtQuick 2.12
import QtQml.Models 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as Controls
import QtMultimedia 5.12
import org.kde.kirigami 2.5 as Kirigami
import us.parich.wolo 1.0

Kirigami.Page {
    id: root

    Provider {
        id: provider

        onFileLoaded: {
            if (name_ === fileId)
                fileSource = downloadFile(board_, name_)
        }

        onFileProgress: {
            if (name_ === fileId)
                root.progress = progress_
        }
    }

    onFocusChanged: {
        if (!focus)
            video.pause()
    }

    title: fileId

    property string boardId: ''
    property string fileSource: ''
    property string file: ''
    property string ext: ''
    readonly property string fileId: file + ext
    onFileIdChanged: {
        fileSource = ''
        if (file !== '' && ext !== '')
            provider.requestFile(root.boardId, root.fileId)
    }

    actions {
        main: Kirigami.Action {
            text: "Save"
            icon.name: "document-save"
            onTriggered: {
                provider.saveFile(root.boardId, root.fileId)
            }
        }
        right: Kirigami.Action {
            text: video.paused() ? "Play" : "Pause"
            icon.name: video.paused(
                           ) ? "media-playback-pause" : "media-playback-start"
            onTriggered: {
                if (typeof UBUNTU_TOUCH !== 'undefined') {
                    if (video.paused())
                        video.play()
                    if (video.playing())
                        video.pause()
                } else
                    Qt.openUrlExternally("video://%1".arg(fileSource.replace('file://', '')))
            }
        }

        left: Kirigami.Action {
            text: video.muted ? "Unmute" : "Mute"
            icon.name: video.muted ? "volume-min" : "volume-max"
            onTriggered: {
                video.muted = !video.muted
            }
        }
        contextualActions: [
            Kirigami.Action {
                text: "Share"
                icon.name: "share"
            }
        ]
    }

    Video {
        visible: typeof UBUNTU_TOUCH === 'undefined'
        id: video
        source: root.fileSource
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        fillMode: VideoOutput.PreserveAspectFit

        width: parent.availableWidth
        height: parent.availableHeight
        anchors.fill: parent

        autoLoad: true
        autoPlay: true
        loops: MediaPlayer.Infinite
        muted: true
    }
    Image {
        visible: typeof UBUNTU_TOUCH !== 'undefined'
        source: 'https://i.4cdn.org/%1/%2s.jpg'.arg(boardId).arg(file)
        anchors.fill: parent
        width: parent.width
        height: parent.height
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        fillMode: Image.PreserveAspectFit
    }
}
