import QtQuick 2.12
import QtQml.Models 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.5 as Kirigami
import us.parich.wolo 1.0

Kirigami.Page {
    id: root

    Provider {
        id: provider

        onFileLoaded: {
            if (name_ === fileId)
                fileSource = downloadFile(board_, name_)
        }

        onFileProgress: {
            if (name_ === fileId)
                root.progress = progress_
        }
    }

    title: fileId

    property string boardId: ''
    property string fileSource: ''
    property string file: ''
    property string ext: ''
    readonly property string fileId: file + ext
    readonly property bool isPng: ext !== '' && ext !== '.gif'
    readonly property bool isGif: ext === '.gif'

    onFileIdChanged: {
        fileSource = ''
        if (file !== '' && ext !== '')
            provider.requestFile(root.boardId, root.fileId)
    }

    actions {
        main: Kirigami.Action {
            text: "Save"
            icon.name: "document-save"
            onTriggered: {
                provider.saveFile(root.boardId, root.fileId)
            }
        }
        contextualActions: [
            Kirigami.Action {
                text: "Share"
                icon.name: "share"
            }
        ]
    }

    onFocusChanged: {
        if (!focus) {
            png.anchors.fill = png.parent
            png.scale = 1
            png.rotation = 0
            gif.anchors.fill = png.parent
            gif.scale = 1
            gif.rotation = 0
        }
    }

    Image {
        id: png
        visible: isPng
        source: fileSource
        anchors.fill: parent
        scale: 1
        rotation: 0
        fillMode: Image.PreserveAspectFit

    MouseArea {
        id: mouse

        onDoubleClicked: {
            png.anchors.fill = png.parent
            png.scale = 1
            png.rotation = 0
        }
    }
    }
    AnimatedImage {
        id: gif
        visible: isGif
        source: fileSource
        anchors.fill: parent
        scale: 1
        rotation: 0
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

        fillMode: Image.PreserveAspectFit
    }

    PinchHandler {
        id: pinch
        target: isPng ? png : gif
        maximumRotation: 0
        minimumRotation: 0

        onActiveChanged: {
            pageStack.interactive = !pinch.active
        }
    }
}
