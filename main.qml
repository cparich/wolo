import QtQuick 2.12
import org.kde.kirigami 2.5 as Kirigami
import QtQuick.Controls 2.12 as Controls
import us.parich.wolo 1.0

Kirigami.ApplicationWindow {
    id: root

    title: "Wolo"

    Provider {
        id: provider
    }

    globalDrawer: Kirigami.GlobalDrawer {
        title: "Wolo"
        bannerImageSource: Qt.resolvedUrl("logo.svg")
        isMenu: typeof UBUNTU_TOUCH === 'undefined'
        actions: [
            Kirigami.Action {
                text: "Clear cache..."
                icon.name: "drive-harddisk-symbolic"
                onTriggered: {
                    provider.clearCache()
                }
            }
        ]
     }

    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: Qt.resolvedUrl("Boards.qml")
}
