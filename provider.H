#include <QByteArray>
#include <QMovie>
#include <QQuickImageProvider>

class provider_t : public QObject
{
	Q_OBJECT
public:
	~provider_t () override;

	provider_t ();

	Q_INVOKABLE QString downloadFile (QString const &board_, QString const &name_);

	Q_INVOKABLE void requestFile (QString const &board_, QString const &name_);
	Q_INVOKABLE void saveFile (QString const &board_, QString const &name_);
	Q_INVOKABLE void clearCache ();

signals:
	void fileLoaded (QString const &board_, QString const &name_);
	void fileProgress (QString const &board_, QString const &name_, float progress_);
};
