TEMPLATE = app
TARGET = wolo

QT += quick

CONFIG += c++17

QML_IMPORT_PATH =

QML_DESIGNER_IMPORT_PATH =

HEADERS += *.H
SOURCES += *.C

!UBUNTU_TOUCH {
	QT += multimedia
}

UBUNTU_TOUCH {
	DEFINES += Q_OS_UBUNTU_TOUCHA
	target.path = /
	click_files.path = /
	click_files.files = $$PWD/packaging/ut/*
	INSTALLS += click_files
}

RESOURCES += qml.qrc

INSTALLS += target
