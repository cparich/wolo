import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.5 as Kirigami
import us.parich.wolo 1.0

Kirigami.ScrollablePage {
    id: root

    title: '/' + boardId + '/'

    property string boardId: ''
    property string threadId: ''
    property bool threadSelected: false

    onBoardIdChanged: {
        threadId = ''

        if (threadSelected)
            pageStack.pop ()

        threadSelected = false
    }

    refreshing: true

    supportsRefreshing: true
    onRefreshingChanged: {
        if (refreshing)
            catalog.refresh()
    }

    CatalogRepo {
        id: catalog
        boardId: root.boardId
        onBoardIdChanged: catalog.refresh()
        onThreadListChanged: root.refreshing = false
        onProgress: root.progress = progress_
    }

    actions {
        right: Kirigami.Action {
            icon.name: "document-edit"
            text: "New Thread"
            onTriggered: showPassiveNotification("Left action triggered")
        }
        main: Kirigami.Action {
            icon.name: "view-refresh"
            text: "Refresh"
            onTriggered: catalog.refresh ()
        }
    }

    Kirigami.CardsGridView {
        id: grid

        model: catalog.threadList
        maximumColumnWidth: Kirigami.Units.gridUnit * 20
        minimumColumnWidth: Kirigami.Units.gridUnit * 10

        delegate: Kirigami.Card {

            property Post slug: model.modelData

            Layout.fillHeight: true
            Layout.margins: 9
            id: listItem

            visible: !slug.hidden

            height: grid.cellHeight

            showClickFeedback: true

            onClicked: {
                threadId = slug.postId
                pageStack.push(Qt.resolvedUrl('Thread.qml'), {boardId: root.boardId, threadId: root.threadId})
                threadSelected = true;

                grid.currentIndex = model.index
            }

            banner {
                source: slug.deleted ?
                            "https://s.4cdn.org/image/filedeleted.gif" :
                            "https://i.4cdn.org/%1/%2s.jpg".arg(root.boardId).arg(slug.imageId)
                title: slug.subject
                titleLevel: 3
                titleWrapMode: Text.NoWrap
                titleAlignment: Qt.AlignBottom | Qt.AlignHCenter
                fillMode: Image.PreserveAspectCrop
            }

            contentItem: Controls.Label {
                text: slug.body
                wrapMode: Text.WordWrap
                clip: true
                height: 150
            }

//            hiddenActions: [
//                Kirigami.Action {
//                    icon.name: "view-hidden"
//                    text: "Hide thread"
//                    onTriggered: slug.hidden = true
//                }
//            ]
        }
    }
}
