import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.5 as Kirigami
import us.parich.wolo 1.0

Kirigami.ScrollablePage {
    id: root

    title: "Boards"

    property bool boardSelected: false
    property string selectedBoard: ''

    BoardRepo {
        id: boardRepo

        onFirstLoadFinished: {
            selectedBoard = favorite_
            pageStack.push(Qt.resolvedUrl('Catalog.qml', {boardId: favorite_}))
            boardSelected = true
        }

        onProgress: root.progress = progress_
    }

    Component {
        id: boardItem
        Kirigami.SwipeListItem {

            property Board board: model.display
            id: listItem

            highlighted: board.favorite

            onClicked: {
                pageStack.push(Qt.resolvedUrl('Catalog.qml'), {boardId: board.boardName})
            }

            RowLayout {

                Kirigami.ListItemDragHandle {
                    id: handle

                    width: height
                    listItem: listItem
                    listView: boardList
                    onMoveRequested: boardRepo.move (oldIndex, newIndex)
                    Layout.fillHeight: true
                    Layout.fillWidth: false
                }

                Kirigami.Heading {
                    Layout.fillWidth: false
                    Layout.fillHeight: true
                    text: '/'+board.boardName+'/'
                    level: 1
                }
                Controls.Label {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    text: board.boardDescription
                }
            }

            actions: [
                Kirigami.Action {
                    icon.name: board.favorite ? "pinned" : "unpinned"
                    text: "Mark favorite"
                    onTriggered: {
                        showPassiveNotification ('Marked /%1/ favorite'.arg(board.boardDescription))
                        board.favorite = !board.favorite
                    }
                    visible: true
                }
            ]
        }
    }

    ListView {
        id: boardList

        model: boardRepo
        anchors.fill: parent

        delegate: Kirigami.DelegateRecycler {
            width: boardList.width
            sourceComponent: boardItem
        }

        moveDisplaced: Transition {
            YAnimator {
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }
    }
}
